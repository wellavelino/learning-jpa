package br.com.avelino.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Book
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    @ManyToOne
    @JoinColumn(name = "id_publishingHouse")
    private PublishingHouse publishingHouse;
    @ManyToMany
    @JoinTable(name = "book_author", joinColumns =
    { @JoinColumn(name = "id_book") }, inverseJoinColumns =
    { @JoinColumn(name = "id_author") })
    private List<Author> autores;
    @ManyToOne
    @JoinColumn(name = "id_category")
    private Category category;
    private String Isbn;
    @OneToMany(mappedBy = "book")
    private List<Ioan> ioan;
}
