package br.com.avelino.entidades;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Ioan {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private LocalDate ioanDate;
	private LocalDate expectedDate;
	private LocalDate returnDate;
	@ManyToOne
	@JoinColumn(name = "id_book")
	private Book book;
	@ManyToOne
	@JoinColumn(name = "id_reader")
	private Reader reader;
	@ManyToOne
	@JoinColumn(name = "id_oficialLibrary")
	private OficialLibrary oficialLibrary;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public LocalDate getIoanDate() {
		return ioanDate;
	}
	public void setIoanDate(LocalDate ioanDate) {
		this.ioanDate = ioanDate;
	}
	public LocalDate getExpectedDate() {
		return expectedDate;
	}
	public void setExpectedDate(LocalDate expectedDate) {
		this.expectedDate = expectedDate;
	}
	public LocalDate getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}
	public OficialLibrary getOficialLibrary() {
		return oficialLibrary;
	}
	public void setOficialLibrary(OficialLibrary oficialLibrary) {
		this.oficialLibrary = oficialLibrary;
	}
	
	
	
	
}
