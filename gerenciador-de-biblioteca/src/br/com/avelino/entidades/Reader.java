package br.com.avelino.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Reader extends Person
{

    private String classroom;
    private String registration;
    @OneToMany(cascade =
    { CascadeType.ALL })
    @JoinColumn(name = "id_reader")
    private List<Telephone> telephones = new ArrayList<Telephone>();
    @OneToOne(cascade =
    { CascadeType.ALL })
    @JoinColumn(name = "id_address")
    private Address address;

    public String getClassroom()
    {
	return classroom;
    }

    public void setClassroom(String classroom)
    {
	this.classroom = classroom;
    }

    public String getRegistration()
    {
	return registration;
    }

    public void setRegistration(String registration)
    {
	this.registration = registration;
    }

    public List<Telephone> getTelephones()
    {
	return telephones;
    }

    public void setTelephones(List<Telephone> telephones)
    {
	this.telephones = telephones;
    }

    public Address getAddress()
    {
	return address;
    }

    public void setAddress(Address address)
    {
	this.address = address;
    }

}
