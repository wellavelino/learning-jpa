package br.com.avelino.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class OficialLibrary extends Person
{

    @OneToOne(cascade =
    { CascadeType.ALL })
    @JoinColumn(name = "id_user")
    User user;

    @OneToMany(cascade =
    { CascadeType.ALL })
    @JoinColumn(name = "id_oficialLibrary")
    private List<Telephone> telephones = new ArrayList<Telephone>();
    @OneToOne(cascade =
    { CascadeType.ALL })
    @JoinColumn(name = "id_address")
    private Address address;

    public User getUser()
    {
	return user;
    }

    public void setUser(User user)
    {
	this.user = user;
    }

    public List<Telephone> getTelephones()
    {
	return telephones;
    }

    public void setTelephones(List<Telephone> telephones)
    {
	this.telephones = telephones;
    }

    public Address getAddress()
    {
	return address;
    }

    public void setAddress(Address address)
    {
	this.address = address;
    }

}
