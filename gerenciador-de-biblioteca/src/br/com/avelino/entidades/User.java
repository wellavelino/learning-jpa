package br.com.avelino.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String user;
    private String password;
    private boolean admin = true;
    @OneToOne(mappedBy = "user")
    private OficialLibrary oficialLibrary;

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getUser()
    {
	return user;
    }

    public void setUser(String user)
    {
	this.user = user;
    }

    public String getPassword()
    {
	return password;
    }

    public void setPassword(String password)
    {
	this.password = password;
    }

    public boolean isAdmin()
    {
	return admin;
    }

    public void setAdmin(boolean admin)
    {
	this.admin = admin;
    }

    public OficialLibrary getOficialLibrary()
    {
	return oficialLibrary;
    }

    public void setOficialLibrary(OficialLibrary oficialLibrary)
    {
	this.oficialLibrary = oficialLibrary;
    }

}
