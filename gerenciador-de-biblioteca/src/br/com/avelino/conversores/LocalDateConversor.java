package br.com.avelino.conversores;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/** Utilizando o LocalDate do java 8 e @converter do jpa para fazer conversoes de localdate para .sqlDate **/
@Converter(autoApply = true)
public class LocalDateConversor implements AttributeConverter<LocalDate, Date>
{

    @Override
    public Date convertToDatabaseColumn(LocalDate dateEntity)
    {

	return (dateEntity == null) ? null : Date.valueOf(dateEntity);
    }

    @Override
    public LocalDate convertToEntityAttribute(Date dateDataBase)
    {

	return (dateDataBase == null) ? null : dateDataBase.toLocalDate();
    }

}
